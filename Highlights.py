# -*- coding: utf-8 -*-
__author__ = 'ByungJoo'

from bs4 import BeautifulSoup
import urllib2
import csv
import sys
import time


reload(sys)
sys.setdefaultencoding('utf-8')


jongname = []
shcode = []


def get_CSV():
    with open('StockMaster0821_code.csv','rb') as csvfile:
        csvReader = csv.reader(csvfile,delimiter = ',', quotechar = '|' )
        for row in csvReader:
            #name = row[0].decode('cp949')
            # 종목 Code 를 가지고 오는 Source
            code = row[0][1:]

            shcode.append(code)

    getHighligtsHTML()
    #getTestHTML()


def getTestHTML() :
    res = urllib2.urlopen('http://media.kisline.com/fininfo/mainFininfo.nice?paper_stock='+'036640'+'&nav=4').read()
    financialName = 'NiceFinancial'+'036640'+'.html'

    resFnGuide = urllib2.urlopen('http://comp.fnguide.com/SVO2/ASP/SVD_FinanceRatio.asp?pGB=1&gicode=A'+'036640'+'&cID=&MenuYn=Y&ReportGB=&NewMenuID=104&stkGb=701').read()
    financialNamefnGuide = 'FnGuideFinancial'+'036640'+'.html'

    with open(financialName, 'w') as f:
        f.write(res)

    with open(financialNamefnGuide, 'w') as f:
        f.write(resFnGuide)




def getHighligtsHTML():
    for code in shcode:
        print code

        # 재무정보 관련 Nice
        res = urllib2.urlopen('http://media.kisline.com/fininfo/mainFininfo.nice?paper_stock='+code+'&nav=4').read()
        financialName = 'NiceFinancial'+code+'.html'

        # Highlights 관련 Nice
        #res = urllib2.urlopen('http://media.kisline.com/highlight/mainHighlight.nice?paper_stock='+code+'&nav=4').read()
        #financialName = 'Highlights(Nice)'+code+'.html'


        # fnGUide 재무정보
        resFnguide = urllib2.urlopen('http://comp.fnguide.com/SVO2/ASP/SVD_FinanceRatio.asp?pGB=1&gicode=A'+code+'&cID=&MenuYn=Y&ReportGB=&NewMenuID=104&stkGb=701').read()
        financialNameFnguide = 'FnGuideFinancial'+code+'.html'


        with open(financialName, 'w') as f:
           f.write(res)

        with open(financialNameFnguide, 'w') as f:
           f.write(resFnguide)

if __name__ == '__main__':
    get_CSV()
